<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Craft>
 */
class CraftFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'item_id' => Item::get()->random()->id,
            'item_for_crafting' => Item::get()->random()->id,
            'position' => $this->faker->numberBetween('1', '9'),
        ];
    }
}
