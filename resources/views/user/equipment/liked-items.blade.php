@extends('user.layouts.app')

@section('header')
    {{ __('Liked Items') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item link-primary link-offset-2">
                        <a href="{{ route('user.equipment.main') }}">Main page</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Liked items</li>
                </ol>
            </nav>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="row row-cols-1 row-cols-md-5 g-4">


                    @if(count($items) != 0)
                    <table class="table table-hover w-50">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th></th>

                        </tr>
                        </thead>

                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td class="align-middle"><a href="{{ route('user.items.show', $item) }}">{{ $item->title }}</a></td>
                                <td class="text-center">
                                    <form action="{{ route('user.equipment.like.item', $item) }}" method="post"
                                          class="ms-2">
                                        @method('put')
                                        @csrf
                                        <button type="submit">
                                                <i class="bi bi-heart-fill"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <p class="fs-5">You don't have any liked items</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
