@extends('user.layouts.app')

@section('header')
    {{ __('Equipment') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="container px-4 py-5">
                    <h2 class="pb-2 border-bottom">{{ auth()->user()->name }}</h2>

                    <div class="row row-cols-1 row-cols-md-2 align-items-start g-5 py-5">
                        <div class="col d-flex flex-column align-items-start gap-2">
                            <h3 class="fw-bold">Future</h3>
                            <p class="text-body-secondary">Functional</p>
                            <button type="button" class="btn btn-primary" disabled>To be continue...</button>
                        </div>

                        <div class="col">
                            <div class="row row-cols-1 row-cols-md-2 g-4">

                                <div class="col">
                                    <div class="card text-bg-light mb-3" style="max-width: 18rem;">
                                        <div class="card-header bg-secondary text-white">Liked items</div>
                                        <div class="card-body">
                                            <h5 class="card-title">Total: {{ $totalLikedItems }}</h5>
                                            <a href="{{ route('user.equipment.liked.items') }}" class="btn btn-outline-secondary text-gray-700">Vew all</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="card border-success mb-3" style="max-width: 18rem;">
                                        <div class="card-header bg-success text-white">All comments</div>
                                        <div class="card-body">
                                            <h5 class="card-title">Total: {{ $totalComments }}</h5>
                                            <a href="{{ route('user.comment.index') }}" class="btn btn-outline-success text-gray-700">Vew all</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="card border-warning mb-3" style="max-width: 18rem;">
                                        <div class="card-header bg-warning">All items</div>
                                        <div class="card-body">
                                            <h5 class="card-title">Total: {{ $totalComments }}</h5>
                                            <a href="{{ route('user.equipment.purchased.items') }}" class="btn btn-outline-warning text-gray-700">Vew all</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
