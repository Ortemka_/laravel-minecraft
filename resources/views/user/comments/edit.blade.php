@extends('user.layouts.app')

@section('header')
    {{ __('Edit Comment') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">


                <form action="{{ route('user.comment.update', $comment) }}" class="w-50" method="post" enctype="multipart/form-data">

{{--                    <input type="hidden" name="item_uuid" value="{{ $item->uuid }}">--}}

                    @method('patch')
                    @csrf
                    <x-input
                        type="text"
                        name="message"
                        field="message"
                        placeholder="Message"
                        class="w-full"
                        autocomplete="off"
                        :value="@old('message', $comment->message )">
                    </x-input>

                    <x-button class="mt-6">Save Changes</x-button>

                </form>
            </div>
        </div>
    </div>
@endsection
