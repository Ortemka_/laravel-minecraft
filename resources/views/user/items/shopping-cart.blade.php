@extends('user.layouts.app')

@section('header')
    {{__('Cart')}}
@endsection

@section('content')

    <livewire:shopping-cart/>
@endsection
