@extends('user.layouts.app')

@section('header')
    {{__('Items List')}}
@endsection

@section('content')
    <livewire:item-list/>
@endsection
