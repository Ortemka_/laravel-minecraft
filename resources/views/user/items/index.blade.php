@extends('user.layouts.app')

@section('header')
    {{ __('Items') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="row row-cols-1 row-cols-md-5 g-4">

                    @foreach($items as $item)

                        <div class="col">
                            <div class="card rounded-top">
                                <img src="{{ asset($item->image) }}" class="card-img-top p-4" alt="{{ $item->title }}">
                                <div class="card-body text-center ">
                                    <a href="{{ route('user.items.show', $item->id) }}" class="card-title fs-4 fw-medium fw-bolder stretched-link">{{ $item->title }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>

            <div class="mt-3">
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
