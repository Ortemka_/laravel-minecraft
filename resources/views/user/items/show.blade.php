@extends('user.layouts.app')

@section('header')
    {{__('Items')}}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item link-primary link-offset-2">
                        <a href="{{ route('user.items.index') }}">Items</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        {{ $item->title }}
                    </li>
                </ol>
            </nav>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">

                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2  border-bottom">

                    <div class="d-flex justify-between">
                        <h2 class="font-bold text-4xl ">
                            {{ $item->title }}
                        </h2>
                        @auth()
                            <form action="{{ route('user.equipment.like.item', $item) }}" method="post"
                                  class="ms-2">
                                @method('put')
                                @csrf
                                <button type="submit">
                                    @if(auth()->user()->likedItems->contains($item->id))
                                        <i class="bi bi-heart-fill fw-semibold">{{ $item->likedUsers->count() }}</i>
                                    @else
                                        <i class="bi bi-heart fw-semibold">{{ $item->likedUsers->count() }}</i>
                                    @endif
                                </button>
                            </form>
                        @endauth
                    </div>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <!-- Text on left side of item title  -->
                        </div>
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-md-2 align-items-start g-5 py-5">

                    <!-- Item main information -->
                    <div class="col d-flex flex-column align-items-start gap-2">
                        <img src="{{ asset($item->image) }}" alt="empty_section" width="150" height="150">
                        <p class="mt-2">
                            <b>Type:</b> {{ $item->type->title }}
                        </p>
                        <p class="mt-6" style="text-align: justify; width: 80%">
                            <b>Description:</b> {{ $item->description }}
                        </p>
                    </div>

                    <!-- Item craft  -->
                    <div class="col">
                        <div class="d-flex justify-content-center">
                            @if(!empty($itemsForCraft))
                                <table class="">
                                    <tr>
                                        @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                            <td class="border border-4 border-dark"  background="{{ asset('/storage/items/empty_section.jpg') }}">
                                                <div class="border border-3">
                                                    @if(isset($itemsForCraft[$craftPosition]))
                                                        <img src="{{ asset($itemsForCraft[$craftPosition]->image) }}"
                                                             alt="{{ $itemsForCraft[$craftPosition]->title }}"
                                                             width="80"
                                                             height="80">
                                                    @else
                                                        <img src="{{ asset('/storage/items/empty_section.jpg') }}"
                                                             alt="empty_section"
                                                             width="80"
                                                             height="80">
                                                    @endif
                                                </div>
                                            </td>

                                            @if($craftPosition % 3 == 0)
                                    </tr>
                                    <tr>
                                        @endif
                                        @endfor
                                    </tr>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                @auth
                    <!-- Form for new comment -->
                    <form action="{{ route('user.comment.store', $item) }}" class="w-50 " method="post"
                          enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <x-textarea
                            name="comment"
                            rows="3"
                            field="comment"
                            placeholder="Leave a comment"
                            class="w-full mt-6"
                            :value="@old('comment')">
                        </x-textarea>

                        <x-button class="mt-2">Save a comment</x-button>
                    </form>
                @endauth

                @if(count($item->comments) != 0)
                    <table class="table table-hover mt-6">
                        <thead>
                        <tr>
                            <th>Comments</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($item->comments as $comment)
                            <tr>
                                <td class="align-middle w-75" style="text-align: justify">{{ $comment->message }}</td>
                                <td class="align-middle text-body-secondary ">
                                    <small class="text-body-secondary fw-semibold">{{ $comment->updated_at->diffForHumans() }}</small>
                                </td>
                                <td class="align-middle">
                                    <div class="d-flex align-items-center">
                                        @if(auth()->id() == $comment->user_id)
                                            <form action="{{ route('user.comment.delete', $comment) }}" method="post">

                                                <a href="{{ route('user.comment.edit', $comment) }}" class="btn btn-outline-primary text-gray-700">Edit</a>
                                                <input type="hidden" name="item_id" value="{{ $item->id }}">
                                                @method('delete')
                                                @csrf

                                                <button type="submit" class="btn btn-outline-danger text-gray-700"
                                                        onclick="return confirm('Are you sure you wish to delete your comment ?')">
                                                    Delete
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>
@endsection
