@extends('user.layouts.app')

@section('header')
    {{ __('Items by types') }}
@endsection

@section('content')

    <livewire:item-list-by-types/>
{{--    <div class="py-12">--}}
{{--        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">--}}

{{--            <x-alert-success>--}}
{{--                {{ session('success') }}--}}
{{--            </x-alert-success>--}}

{{--            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">--}}
{{--                <div class="container px-4 py-5">--}}

{{--                        <div class="col-sm-8 py-5 mx-auto">--}}
{{--                            <p>craft</p>--}}
{{--                        </div>--}}

{{--                    <div class="row">--}}
{{--                        @foreach($types as $type)--}}
{{--                            <div class="col">--}}
{{--                                <h1 class="fs-3 mb-2">{{ $type->title }}</h1>--}}

{{--                                <ul class="list-group list-group-horizontal-sm">--}}
{{--                                    @foreach($type->items as $item)--}}
{{--                                        <li class="me-1 mb-1">--}}
{{--                                            <img src="{{ asset($item->image) }}"--}}
{{--                                                 class="card p-1" style="max-width: 70px"--}}
{{--                                                 alt="{{ $item->title }}">--}}
{{--                                        </li>--}}
{{--                                    @if($loop->iteration % 6 ==0 )--}}
{{--                                        </ul>--}}
{{--                                        <ul class="list-group list-group-horizontal-sm">--}}
{{--                                    @endif--}}

{{--                                    @endforeach--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                            @if($loop->iteration % 2 ==0 )--}}
{{--                                </div>--}}
{{--                                <div class="p-4"></div>--}}
{{--                                <div class="row">--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
{{--                    </div>--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

@endsection
