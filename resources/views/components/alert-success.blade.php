@if(session('success'))
    <div class="mb-4 px-4 py-2 bg-success p-2 text-dark text-center bg-opacity-50 rounded-md border-2 border border-success">
        {{ $slot }}
    </div>
@endif
