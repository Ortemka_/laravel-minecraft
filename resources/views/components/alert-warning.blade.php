@if(session('warning'))
    <div class="mb-4 px-4 py-2 bg-warning p-2 text-dark text-center  bg-opacity-50 rounded-md border-2 border border-warning ">
        {{ $slot }}
    </div>
@endif
