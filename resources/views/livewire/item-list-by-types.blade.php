<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

        <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
            <div class="container px-4">

                @if(session('success') || session('warning'))
                    <x-alert-success>
                        {{ session('success') }}
                    </x-alert-success>

                    <x-alert-warning>
                        {{ session('warning') }}
                    </x-alert-warning>
                @else
                    <div class="mb-4 px-4 py-2 p-2 text-white text-center rounded-md border border-white border-2 ">
                        _
                    </div>
                @endif

                @if($selectedItem != null)
                <div class="row" style="height: 190px">
                    <div class="col text-start mt-6">
                        <p class="fs-1">{{ $selectedItem->title }}</p>
{{--                        <button type="button" class="btn btn-outline-primary text-gray-700"   --}}  {{-- livewire problem. when buying an item with crafting, an error occurs --}}
{{--                                wire:click="addToCart({{ $selectedItem->id }})">Buy {{ $selectedItem->price }} <i class="bi bi-coin"></i>--}}
{{--                        </button>--}}
                    </div>
                    <div class="col text-center">
                        @if(count($selectedItem->crafts) != 0)
                            <table>
                                <tr>
                                    @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                        <td class="border border-4 border-dark"  background="{{ asset('/storage/items/empty_section.jpg') }}">
                                            <div class="border border-3">
                                                @if(isset($itemsForCraft[$craftPosition]))
                                                    <img src="{{ asset($itemsForCraft[$craftPosition]->image) }}"
                                                         alt="{{ $itemsForCraft[$craftPosition]->title }}"
                                                         width="50"
                                                         height="50">
                                                @else
                                                    <img src="{{ asset('/storage/items/empty_section.jpg') }}"
                                                         alt="empty_section"
                                                         width="50"
                                                         height="50">
                                                @endif
                                            </div>
                                        </td>

                                        @if($craftPosition % 3 == 0)
                                </tr>
                                <tr>
                                    @endif
                                    @endfor
                                </tr>
                            </table>
                        @endif
                    </div>
                    <div class="col">
                        <img src="{{ asset($selectedItem->image) }}" class="mx-auto p-1"
                             alt="empty_section" width="150" height="150" style="margin-top: 5%">
                    </div>
                </div>


                @else
                    <div class="row" style="height: 190px"></div>
                @endif

                <div class="p-4"></div>

                <div class="row">
                    @foreach($types as $type)
                        <div class="col">
                            <h1 class="fs-3 mb-2">{{ $type->title }}</h1>

                            <ul class="list-group list-group-horizontal-sm">
                                @foreach($type->items as $item)
                                    <li class="me-1 mb-1" wire:click="$emit('itemSelected', {{ $item->id }})">
                                        @if($selectedItem != null && $selectedItem->id == $item->id)
                                            <img src="{{ asset($item->image) }}"
                                                 class="card p-1 border-3 border-danger"
                                                 style="max-width: 70px"
                                                 alt="{{ $item->title }}">
                                        @else
                                            <img src="{{ asset($item->image) }}"
                                                 class="card p-1"
                                                 style="max-width: 70px"
                                                 alt="{{ $item->title }}">
                                        @endif
                                    </li>
                                    @if($loop->iteration % 6 ==0 )
                            </ul>
                            <ul class="list-group list-group-horizontal-sm">
                                @endif

                                @endforeach
                            </ul>
                        </div>
                        @if($loop->iteration % 2 ==0 )
                        </div>
                        <div class="p-4"></div>
                        <div class="row">
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
