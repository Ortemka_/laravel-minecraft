<div>

    <div class="flex justify-center my-6">

        <div class="flex flex-col w-full p-8 text-gray-800 bg-white shadow-lg pin-r pin-y md:w-4/5 lg:w-4/5">

            @if(session('success') || session('warning'))
                <x-alert-success>
                    {{ session('success') }}
                </x-alert-success>

                <x-alert-warning>
                    {{ session('warning') }}
                </x-alert-warning>
            @else
                <div class="mb-4 px-4 py-2 p-2 text-white text-center rounded-md border border-white border-2 ">
                    _
                </div>
            @endif

            <div class="flex-1">
                <table class="w-full text-sm lg:text-base" cellspacing="0">
                    <thead>
                    <tr class="h-12 uppercase">
                        <th class="hidden md:table-cell"></th>
                        <th class="text-left">Product</th>
                        <th class="lg:text-right text-left pl-5 lg:pl-0">
                            <span class="lg:hidden" title="Quantity">Qtd</span>
                            <span class="hidden lg:inline">Quantity</span>
                        </th>
                        <th class="hidden text-right md:table-cell">Unit price</th>
                        <th class="text-right">Total price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cartItems as $cartItem)
                        <tr>
                            <td class="hidden pb-4 md:table-cell ">
                                <a href="{{ route('user.items.show', $cartItem->item->id) }}">
                                    <img src="{{ $cartItem->item->image }}" class="w-20 border border-2 rounded p-1" alt="Thumbnail" />
                                </a>
                            </td>
                            <td>
                                <p class="mb-2 md:ml-4">{{ $cartItem->item->title}}</p>
                                <button type="submit" class="md:ml-4 text-red-700" wire:click="removeItem({{ $cartItem->id }})">
                                    <small>(Remove item)</small>
                                </button>
                            </td>
                            <td class="justify-center md:justify-end md:flex mt-6">
                                <div class="w-20 h-10">
                                    <div class="custom-number-input h-10 w-32">
                                        <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
                                            <button class="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer outline-none"
                                                    wire:click="decrementQty({{ $cartItem->id }})">
                                                <span class="m-auto text-2xl font-thin">−</span>
                                            </button>
                                            <span class="p-2">{{ $cartItem->quantity}}</span>
                                            <button class="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer"
                                                    wire:click="incrementQty( {{ $cartItem->id }} )">
                                                <span class="m-auto text-2xl font-thin">+</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="hidden text-right md:table-cell">
                                <span class="text-sm lg:text-base font-medium">
                                    {{ $cartItem->item->price }} <i class="bi bi-coin"></i>
                                </span>
                            </td>
                            <td class="text-right">
                                <span class="text-sm lg:text-base font-medium">
                                    {{ $cartItem->item->price * $cartItem->quantity }} <i class="bi bi-coin"></i>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <hr class="pb-6 mt-6" />
                <div class="my-1 mt-1 -mx-2 lg:flex">
                    <div class="lg:px-2 lg:w-1/2"></div>
                    <div class="lg:px-2 lg:w-1/2">
                        <div class="">
                            <div class="flex justify-between pt-1 border-b">
                                <div class="lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-center text-gray-800">
                                    Total
                                </div>
                                <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                    {{ $total }} <i class="bi bi-coin"></i>
                                </div>
                                <div class="lg:px-4 lg:py-2 m-2 mb-2 lg:text-lg font-bold text-center text-gray-900">
                                    <button type="button" class="btn btn-outline-success text-gray-700"
                                        wire:click="confirmPurchase">Success</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

