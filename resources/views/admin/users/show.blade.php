@extends('admin.layouts.app')

@section('header')
    {{ __('Edit Item') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>
            <div class="d-flex justify-content-between">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item link-primary link-offset-2">
                            <a href="{{ route('admin.users.index') }}">All users</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{ $user->name }}</li>
                    </ol>
                </nav>

                <a class="btn btn-outline-primary text-gray-700"
                   href="{{ route('admin.users.edit', $user->id) }}">Edit {{ $user->name }}</a>
            </div>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="container px-4 py-5">
                    <h2 class="pb-2 border-bottom">{{ $user->name }}</h2>

                    <div class="row row-cols-1 row-cols-md-2 align-items-start g-5 py-5">
                        <div class="col d-flex flex-column align-items-start gap-2">
                            <h3 class="fw-bold">Future</h3>
                            <p class="text-body-secondary">Functional</p>
                            <button type="button" class="btn btn-primary" disabled>To be continue...</button>
                        </div>

                        <div class="col">
                            <div class="row row-cols-1 row-cols-md-2 g-4">

                                <div class="col">
                                    <div class="card text-bg-light mb-3" style="max-width: 18rem;">
                                        <div class="card-header">User liked items</div>
                                        <div class="card-body">
                                            <h5 class="card-title">Total: {{ $totalLikedItems }}</h5>
                                            <a href="{{ route('admin.users.liked.items', $user) }}" class="btn btn-outline-secondary text-gray-700">Vew all</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="card border-success mb-3" style="max-width: 18rem;">
                                        <div class="card-header">All user comments</div>
                                        <div class="card-body">
                                            <h5 class="card-title">Total: {{ $totalComments }}</h5>
                                            <a href="{{ route('admin.users.comments', $user) }}" class="btn btn-outline-success text-gray-700">Vew all</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
