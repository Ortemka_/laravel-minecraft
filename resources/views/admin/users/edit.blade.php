@extends('admin.layouts.app')

@section('header')
    {{ __('Edit Item') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item link-primary link-offset-2">
                        <a href="{{ route('admin.users.index') }}">All users</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">{{ $user->name }}</li>
                </ol>
            </nav>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">

                <form action="{{ route('admin.users.update', $user) }}" class="w-50" method="post" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <x-input
                        type="text"
                        name="name"
                        field="name"
                        placeholder="Name"
                        class="w-full"
                        autocomplete="off"
                        :value="@old('name', $user->name )">
                    </x-input>

                    <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 w-full mt-2"
                            name="role"
                            aria-label="Default select example">
                        @foreach($roles as $id => $role)
                            <option value="{{ $id }}" {{ $id == $user->role || $id == old('role')  ? 'selected' : '' }}>{{ $role }}</option>
                        @endforeach
                    </select>

                    @error('role')
                    <div class="text-red-600 text-sm">{{ $message }}</div>
                    @enderror


                    <x-input
                        type="text"
                        name="email"
                        field="email"
                        placeholder="Email"
                        class="w-full mt-2"
                        autocomplete="off"
                        :value="@old('email', $user->email )">
                    </x-input>

                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <x-button class="mt-6">Save User</x-button>

                </form>
            </div>
        </div>
    </div>
@endsection
