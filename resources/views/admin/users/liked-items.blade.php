@extends('admin.layouts.app')

@section('header')
    {{ __('Liked Items') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item link-primary link-offset-2">
                        <a href="{{ route('admin.users.show', $user->id) }}">{{ $user->name }}</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Liked items</li>
                </ol>
            </nav>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="row row-cols-1 row-cols-md-5 g-4">


                    @if(count($items) != 0)
                        <table class="table table-hover w-50">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td class="align-middle">
                                        <a href="{{ route('admin.items.show', $item) }}">{{ $item->title }}</a>
                                    </td>
                                    <td class="text-center">
                                        <i class="bi bi-heart-fill"></i>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="fs-5">This user haven't any liked items</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
