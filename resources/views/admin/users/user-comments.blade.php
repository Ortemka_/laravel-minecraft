@extends('admin.layouts.app')

@section('header')
    {{ __('Equipment') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item link-primary link-offset-2">
                        <a href="{{ route('admin.users.show', $user->id) }}">{{ $user->name }}</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">All comments</li>
                </ol>
            </nav>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                @if(count($comments) != 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Comment text</th>
                            <th></th>
                            <th></th>

                        </tr>
                        </thead>

                        <tbody>
                        @foreach($comments as $comment)
                            <tr>
                                <td class="align-middle w-75" style="text-align: justify">{{ $comment->message }}</td>
                                <td class="align-middle text-body-secondary ">
                                    <small class="text-body-secondary fw-semibold">{{ $comment->updated_at->diffForHumans() }}</small>
                                </td>
                                <td class="align-middle">
                                    <form action="{{ route('admin.comments.delete', $comment) }}" method="post">
                                        <input type="hidden" name="item_id" value="{{ $comment->item_id }}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger text-gray-700"
                                                onclick="return confirm('Are you sure you wish to delete this user forever !?')">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                @else
                    <p class="fs-5">This user haven't written any comments</p>
                @endif
            </div>
        </div>
    </div>
@endsection

