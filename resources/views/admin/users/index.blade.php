@extends('admin.layouts.app')

@section('header')
    {{ ('Users') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>


            <a href="{{ route('admin.users.create') }}" class="btn btn-primary">+ New User</a>
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4 w-75">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th class="text-center">Action</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td class="align-middle">{{ $user->id }}</td>
                            <td class="align-middle">{{ $user->name }}</td>
                            <td class="align-middle">{{ $user->email }}</td>
                            <td class="align-middle">{{ isset($user->role) ? $roles[$user->role] : 'default'}}</td>
                            <td class="text-center">
                                <form action="{{ route('admin.users.destroy', $user->id) }}" method="post">

                                    <a class="btn btn-outline-primary text-gray-700"
                                       href="{{ route('admin.users.show', $user->id) }}">See user</a>
                                    <a class="btn btn-outline-primary text-gray-700"
                                       href="{{ route('admin.users.edit', $user->id) }}">Edit</a>
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger text-gray-700"
                                            onclick="return confirm('Are you sure you wish to delete this user forever !?')">
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
