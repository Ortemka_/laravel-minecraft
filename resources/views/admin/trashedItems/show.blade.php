@extends('admin.layouts.app')

@section('header')
    {{__('Trash Item')}}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <div class="flex">

                    <p class="opacity-70">
                        <strong> Deleted at: </strong> {{ $item->deleted_at->diffForHumans() }}
                    </p>

                    <form action="{{ route('admin.trashed.update', $item) }}" method="post" class="ml-auto">
                        @method('patch')
                        @csrf
                        <button type="submit" class="btn btn-primary bg-primary text-white">Restore Item</button>
                    </form>

                    <form action="{{ route('admin.trashed.destroy', $item) }}" method="post">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger ml-4" onclick="return confirm('Are you sure you wish to delete this item forever !?')">
                            Delete Forever
                        </button>
                    </form>

            </div>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4 ">

                <h2 class="font-bold text-4xl ">
                    {{ $item->title }}
                </h2>

                <img src="{{ asset($item->image) }}"
                     alt="empty_section"
                     width="80"
                     height="80">

                <p class="mt-2">
                    Type: {{ $item->type->title }}
                </p>
                <p class="mt-6">
                    Description: {{ $item->description }}
                </p>

                @if(!empty($itemsForCraft))
                    <table class="mt-3">
                        <tr>
                            @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                <td class="border border-4 border-dark" background="{{ asset('/storage/items/empty_section.jpg') }}">
                                    <div class="border border-3">
                                        @if(isset($itemsForCraft[$craftPosition]))
                                            <img src="{{ asset($itemsForCraft[$craftPosition]->image) }}"
                                                 alt="{{ $itemsForCraft[$craftPosition]->title }}"
                                                 width="80"
                                                 height="80">
                                        @else
                                            <img src="/storage/items/empty_section.jpg"
                                                 alt="empty_section"
                                                 width="80"
                                                 height="80">
                                        @endif
                                    </div>
                                </td>

                                @if($craftPosition % 3 == 0)
                                    </tr>
                                    <tr>
                                @endif

                            @endfor
                        </tr>

                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

