@extends('admin.layouts.app')

@section('header')
    {{__('Admin Items')}}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            @if(request()->routeIs('admin.items.index'))
                <a href="{{ route('admin.items.create.step.one') }}" class="btn btn-primary">+ New Item</a>
            @endif

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                <div class="row row-cols-1 row-cols-md-5 g-4">

                    @forelse($items as $item)

                        <div class="col">
                            <div class="card rounded-top">
                                <img src="{{ asset($item->image) }}" class="card-img-top p-4" alt="{{ $item->title }}">
                                <div class="card-body text-center ">
                                    <a
                                        @if(request()->routeIs('admin.items.index'))
                                        href="{{ route('admin.items.show', $item) }}"
                                        @else
                                        href="{{ route('admin.trashed.show', $item) }}"
                                        @endif
                                        class="card-title fs-4 fw-medium fw-bolder stretched-link">{{ $item->title }}</a>
                                </div>
                            </div>
                        </div>
                        @empty
                            @if(request()->routeIs('items.index'))
                                <p>You have no items</p>
                            @else
                                <p>No items in trash</p>
                            @endif
                        @endforelse

                </div>
            </div>

            <div class="mt-3">
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
