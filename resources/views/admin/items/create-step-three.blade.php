@extends('admin.layouts.app')

@section('header')
    {{ __('New item: Data checking') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">

                <form action="{{ route('admin.items.create.step.three.post') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div
                        class="mb-3 mt-6 w-50 rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                        <label for="image" class="form-label">Load image</label>
                        <input class="form-control" type="file" name="image" id="image">
                    </div>
                    @error('image')
                    <div class="text-red-600 text-sm">{{ $message }}</div>
                    @enderror

                    <table class="table w-50">
                        <tr>
                            <td>Title:</td>
                            <td><strong>{{ $item->title }}</strong></td>
                        </tr>
                        <tr>
                            <td>Description:</td>
                            <td><strong>{{ $item->description }}</strong></td>
                        </tr>
                        <tr>
                            <td>Type:</td>
                            <td><strong>{{ $item->type_id }}</strong></td>
                        </tr>
                    </table>

                    @if($craftExist)
                        <a href="{{ route('admin.items.create.step.two') }}" class="mt-6 btn btn-secondary me-2"><< Previous</a>
                    @else
                        <a href="{{ route('admin.items.create.step.one') }}" class="mt-6 btn btn-secondary me-2"><< Previous</a>
                    @endif

                    <button type="submit" class="mt-6 btn btn-success bg-success text-white">Submit</button>
                    <a href="{{ route('admin.items.create.cancel') }}" class="ml-12 mt-6 btn btn-danger">Cancel item creation</a>

                </form>

            </div>
        </div>
    </div>
@endsection
