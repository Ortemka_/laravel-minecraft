@extends('admin.layouts.app')

@section('header')
    {{ __('New item: Crafting recipe') }}
@endsection

@section('content')

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 ">
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">

                <x-alert-warning>
                    {{ session('warning') }}
                </x-alert-warning>

                <form action="{{ route('admin.items.create.step.two.post') }}" method="post">

                    @csrf

                    <p class="fs-3 ml-4">Specify the items needed for crafting</p>

                    <table class="table mt-6 w-50 table-borderless">
                        <tbody class="mt-4">
                        <tr>
                            @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                <td>
                                    <select class="form-select border border-4 p-2 text-center rounded-4"
                                            name="{{ 'position_' . $craftPosition }}">
                                        <option value="-1"></option>
                                        @foreach($itemsForCraft as $itemForCraft)

                                            @if(isset($oldItemsForCraft[$craftPosition]) && $itemForCraft->id == $oldItemsForCraft[$craftPosition])
                                                <option value="{{ $itemForCraft->id }}" selected>{{ $itemForCraft->title }}</option>
                                            @else
                                                <option value="{{ $itemForCraft->id }}">{{ $itemForCraft->title }}</option>
                                            @endif

                                        @endforeach
                                    </select>
                                </td>

                                @if($craftPosition % 3 == 0)
                                    </tr>
                                    <tr>
                                @endif

                            @endfor
                        </tr>

                        </tbody>
                    </table>

                    <a href="{{ route('admin.items.create.step.one') }}" class="mt-6 btn btn-secondary me-2"><< Previous</a>

                    <button type="submit" class="mt-6 btn btn-primary bg-primary text-white">Next Step >></button>


                </form>
            </div>
        </div>
    </div>
@endsection
