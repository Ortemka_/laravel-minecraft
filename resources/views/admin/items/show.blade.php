@extends('admin.layouts.app')

@section('header')
    {{__('Item Description')}}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <x-alert-success>
                {{ session('success') }}
            </x-alert-success>

            <div class="d-flex justify-content-between">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item link-primary link-offset-2">
                            <a href="{{ route('admin.items.index') }}">Items</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            {{ $item->title }}
                        </li>
                    </ol>
                </nav>

                <form action="{{ route('admin.items.destroy', $item) }}" method="post">
                    <a href="{{ route('admin.items.edit', $item) }}" class="btn btn-primary bg-primary text-white">Edit</a>

                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger ml-4"
                            onclick="return confirm('Are you sure you wish to move this item to trash !?')">
                        Move to trash
                    </button>
                </form>
            </div>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">


                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2  border-bottom">

                    <div class="d-flex justify-between">
                        <h2 class="font-bold text-4xl ">
                            {{ $item->title }}
                        </h2>
                        <i class="bi bi-heart fw-semibold ml-1">{{ $item->likedUsers->count() }}</i>
                    </div>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <!-- Text on left side of item title  -->
                        </div>
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-md-2 align-items-start g-5 py-5">

                    <!-- Item main information -->
                    <div class="col d-flex flex-column align-items-start gap-2">
                        <img src="{{ asset($item->image) }}" alt="empty_section" width="150" height="150">
                        <p class="mt-2">
                            <b>Type:</b> {{ $item->type->title }}
                        </p>
                        <p class="mt-6" style="text-align: justify; width: 80%">
                            <b>Description:</b> {{ $item->description }}
                        </p>
                    </div>

                    <!-- Item craft  -->
                    <div class="col">
                        <div class="d-flex justify-content-center">
                            @if(!empty($itemsForCraft))
                                <table class="">
                                    <tr>
                                        @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                            <td class="border border-4 border-dark"  background="{{ asset('/storage/items/empty_section.jpg') }}">
                                                <div class="border border-3">
                                                    @if(isset($itemsForCraft[$craftPosition]))
                                                        <img src="{{ asset($itemsForCraft[$craftPosition]->image) }}"
                                                             alt="{{ $itemsForCraft[$craftPosition]->title }}"
                                                             width="80"
                                                             height="80">
                                                    @else
                                                        <img src="{{ asset('/storage/items/empty_section.jpg') }}"
                                                             alt="empty_section"
                                                             width="80"
                                                             height="80">
                                                    @endif
                                                </div>
                                            </td>

                                            @if($craftPosition % 3 == 0)
                                                </tr>
                                                <tr>
                                            @endif
                                        @endfor
                                    </tr>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg mt-4">
                @if(count($item->comments) != 0)
                    <table class="table table-hover mt-6">
                        <thead>
                        <tr>
                            <th>Comments</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($item->comments as $comment)
                            <tr>
                                <td class="align-middle w-100" style="text-align: justify; max-width: 70%">{{ $comment->message }}</td>
                                <td class="align-middle text-body-secondary">
                                    <small class="text-body-secondary fw-semibold">{{ $comment->updated_at->diffForHumans() }}</small>
                                </td>
                                <td class="align-middle">
                                    <div class="d-flex align-items-center">
                                        @if(auth()->user()->role == 0)
                                            <form action="{{ route('admin.comments.delete', $comment) }}" method="post">
                                                <a href="{{ route('admin.users.show', $comment->user_id) }}"
                                                   class="btn btn-outline-primary  text-gray-700">See user</a>
                                                <input type="hidden" name="item_id" value="{{ $item->id }}">
                                                @method('delete')
                                                @csrf

                                                <button type="submit" class="btn btn-outline-danger text-gray-700"
                                                        onclick="return confirm('Are you sure you wish to delete user comment ?')">
                                                    Delete
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>
@endsection
