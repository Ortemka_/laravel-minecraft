@extends('admin.layouts.app')

@section('header')
    {{ __('New item: Basic information') }}
@endsection

@section('content')

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">

                <form class="w-50" action="{{ route('admin.items.create.step.one.post') }}" method="post">

                    @csrf

                    <x-input
                        type="text"
                        name="title"
                        field="title"
                        placeholder="Title"
                        class="w-full"
                        autocomplete="off"
                        :value="@old('title', $item->title )">
                    </x-input>

                    <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 w-full mt-6"
                            name="type_id"
                            aria-label="Default select example">
                        <option value="-1">Select type</option>

                        @foreach($types as $type)
                            @if(isset($item->type_id) && $type->id == $item->type_id)
                                <option value="{{ $type->id }}" selected>{{ $type->title }}</option>
                            @else
                                <option value="{{ $type->id }}">{{ $type->title }}</option>
                            @endif
                        @endforeach
                    </select>

                    @error('type_id')
                    <div class="text-red-600 text-sm">{{ $message }}</div>
                    @enderror

                    <x-textarea
                        name="description"
                        rows="10"
                        field="description"
                        placeholder="Description..."
                        class="w-full mt-6"
                        :value="@old('description', $item->description )">
                    </x-textarea>

                    <div class="form-check border rounded-3 mt-2 ">
                        <div class="m-2">
                            <input class="form-check-input" type="checkbox" id="craftExist" {{ $craftExist ? 'checked' : '' }} name="craftExist" value="true" >
                            <label class="form-check-label" for="craft">
                                Add crafting recipe
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary bg-primary text-white  mt-2">Next Step >></button>

                </form>
            </div>
        </div>
    </div>
@endsection
