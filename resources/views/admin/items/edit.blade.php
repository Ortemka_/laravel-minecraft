@extends('admin.layouts.app')

@section('header')
    {{ __('Edit Item') }}
@endsection

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="my-6 p-6 bg-white border-b border-gray-200 shadow-sm sm:rounded-lg">


                <form action="{{ route('admin.items.update', $item) }}" class="w-50" method="post" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <x-input
                        type="text"
                        name="title"
                        field="title"
                        placeholder="Title"
                        class="w-full"
                        autocomplete="off"
                        :value="@old('title', $item->title )">
                    </x-input>

                    <img src="{{ asset($item->image) }}"
                         class="mt-3"
                         alt="empty_section"
                         width="80"
                         height="80">

                    <div class="mb-3 mt-6 w-50 rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                        <label for="image" class="form-label">Change image</label>
                        <input class="form-control" type="file" name="image" id="image">
                    </div>

                    @error('image')
                    <div class="text-red-600 text-sm">{{ $message }}</div>
                    @enderror

                    <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 w-full mt-6"
                            name="type_id"
                            aria-label="Default select example">
                        <option value="-1">Select type</option>
                        @foreach($types as $type)
                                <option value="{{ $type->id }}" {{ $type->id == $item->type_id ? 'selected' : ''}}>{{ $type->title }}</option>
                        @endforeach
                    </select>

                    @error('type_id')
                    <div class="text-red-600 text-sm">{{ $message }}</div>
                    @enderror

                    <x-textarea
                        name="description"
                        rows="5"
                        field="description"
                        placeholder="Description..."
                        class="w-full mt-6"
                        :value="@old('description', $item->description )">
                    </x-textarea>

                        <table class="table mt-4 table-borderless">
                            <tbody class="mt-4">
                            <tr>
                                @for($craftPosition = 1; $craftPosition < 10; $craftPosition++)
                                    <td>
                                        <select class="form-select border border-4 p-2 text-center rounded-4"
                                                name="{{ 'position_' . $craftPosition }}">
                                            <option value="-1"></option>
                                            @foreach($itemsForCraft as  $itemForCraft)
                                                <option value="{{ $itemForCraft->id }}" {{ array_key_exists($craftPosition, $oldItemsForCraft) && $oldItemsForCraft[$craftPosition]->id == $itemForCraft->id ? 'selected' : '' }}>
                                                    {{ $itemForCraft->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>

                                    @if($craftPosition % 3 == 0)
                                </tr>
                                <tr>
                                    @endif

                                @endfor
                            </tr>

                            </tbody>
                        </table>

                    <button type="submit" class="btn btn-primary bg-primary text-white mt-2">Confirm Edit</button>

                    <a href="{{ route('admin.items.index') }}" class="btn btn-danger mt-2 ml-4">Cansel</a>
                </form>
            </div>
        </div>
    </div>
@endsection
