<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserItemPurchased extends Model
{
    use HasFactory;

    protected $table = 'user_item_purchased';

    protected $quarded = [];
}
