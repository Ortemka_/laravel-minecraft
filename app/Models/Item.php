<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['uuid', 'title', 'description', 'price', 'image', 'type_id'];
//    private mixed $crafts; // need to check wy without this don't work getItemsForCraft ItemController ???

//    protected $withCount = ['likedUsers'];

//    public function getRouteKeyName()
//    {
//        return 'id';
//    }

    public function crafts()
    {
        return $this->hasMany(Craft::class, 'item_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likedUsers()
    {
        return $this->belongsToMany(User::class, 'user_item_liked', 'item_id', 'user_id');

    }
}
