<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Craft extends Model
{
    use HasFactory;

    protected $fillable = ['item_id', 'item_for_crafting', 'position'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
