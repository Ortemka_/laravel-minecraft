<?php

namespace App\Http\Controllers\Admin\Comment;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request, Item $item)
    {
        $request->validate([
            'comment' => 'required|min:10',
        ]);

        $comment = new Comment();
        $comment->user_id = Auth::id();
        $comment->message = $request->comment;

        $item->comments()->save($comment);

        return redirect()->route('admin.items.show', $item->id)
            ->with('success', 'Thank for your comment');
    }

    public function edit(Comment $comment, Item $item)
    {
        return view('admin.comments.edit', compact('comment', 'item'));
    }

    public function update(Request $request, Comment $comment)
    {
        $data = $request->validate([
            'message' => 'required',
        ]);

        $comment->update($data);

        return redirect()->route('user.items.show', $comment->item_id)
            ->with('success', 'Your comment has been updated');
    }

    public function delete(Request $request, Comment $comment)
    {
        $comment->delete();

        return redirect()->back()->with('success', 'User comment has been deleted');
    }
}
