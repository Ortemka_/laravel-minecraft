<?php

namespace App\Http\Controllers\Admin\Item;

use App\Http\Controllers\Controller;
use App\Models\Craft;
use App\Models\Item;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function to_route;
use function view;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Item::with('type')->latest('created_at')->paginate(15);

        return view('admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.items.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:20',
            'description' => 'required|max:650',
        ]);

        Item::create([
            'uuid' => Str::uuid(),
            'title' => $request->title,
            'description' => $request->description,
            'type_id' => 1,
        ]);

        return to_route('admin.items.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Item $item)
    {
        $itemsForCraft = self::getItemsForCraft($item);

        return view('admin.items.show', compact('item', 'itemsForCraft'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Item $item)
    {
        $types = Type::all();
        $itemsForCraft = Item::all();
        $oldItemsForCraft = self::getItemsForCraft($item);
        $craftExist = ! empty($oldItemsForCraft);

        return view('admin.items.edit',
            compact('item', 'types', 'itemsForCraft', 'oldItemsForCraft', 'craftExist'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Item $item)
    {
        $data = $request->validate([
            'title' => 'required|max:20',
            'description' => 'required|max:650',
            'image' => ['nullable', 'image:jpg, jpeg, png'],
            'type_id' => 'required|not_in:-1',
        ]);

        if (isset($data['image'])) {
            $imageName = $request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->storeAs('items', $imageName);
            $data['image'] = '/storage/'.$image_path;
        }

        $craftRecipe = $request->validate([
            'position_1' => 'sometimes',
            'position_2' => 'sometimes',
            'position_3' => 'sometimes',
            'position_4' => 'sometimes',
            'position_5' => 'sometimes',
            'position_6' => 'sometimes',
            'position_7' => 'sometimes',
            'position_8' => 'sometimes',
            'position_9' => 'sometimes',
        ]);

        $newItemsForCraft = null;
        foreach ($craftRecipe as $position => $itemId) {
            if ($itemId != -1) {
                $newItemsForCraft[] = [
                    'item_for_crafting' => $itemId,
                    'position' => explode('_', $position)[1],
                ];
            }
        }

        if ($newItemsForCraft == null) {
            Craft::where('item_id', $item->id)->delete();
        } else {
            Craft::where('item_id', $item->id)->delete();
            $item->crafts()->createMany($newItemsForCraft);
        }

        $item->update($data);

        return to_route('admin.items.show', $item)->with('success', ' Item update successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Item $item)
    {
        $item->delete();

        return to_route('admin.items.index')->with('success', ' Item moved to trash');
    }

    protected function getItemsForCraft(Item $item)
    {
        $itemsForCraft = [];

        foreach ($item->crafts()->get() as $craftItem) {
            $itemsForCraft[$craftItem->position] = Item::where('id', $craftItem->item_for_crafting)->firstOrFail();
        }

        return $itemsForCraft;
    }
}
