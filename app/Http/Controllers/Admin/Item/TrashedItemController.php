<?php

namespace App\Http\Controllers\Admin\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use function to_route;
use function view;

class TrashedItemController extends Controller
{
    public function index()
    {
        $items = Item::with('type')->onlyTrashed()->paginate(10);

        return view('admin.trashedItems.index', ['items' => $items]);
    }

    public function show(Item $item)
    {

        $itemsForCraft = self::getItemsForCraft($item);

        return view('admin.trashedItems.show', compact('item', 'itemsForCraft'));
    }

    public function update(Item $item)
    {
        $item->restore();

        return to_route('admin.items.show', $item)->with('success', 'Item restore');
    }

    public function destroy(Item $item)
    {
        $item->forceDelete();

        return to_route('admin.trashed.index')->with('success', 'Item deleted forever');
    }

    protected function getItemsForCraft(Item $item)
    {
        $itemsForCraft = [];

        foreach ($item->crafts()->get() as $craftItem) {
            $itemsForCraft[$craftItem->position] = Item::where('id', $craftItem->item_for_crafting)->firstOrFail();
        }

        return $itemsForCraft;
    }
}
