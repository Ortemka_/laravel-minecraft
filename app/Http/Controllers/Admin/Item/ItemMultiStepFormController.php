<?php

namespace App\Http\Controllers\Admin\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function redirect;
use function view;

class ItemMultiStepFormController extends Controller
{
//-----------------Step One---------------------------------------------------------------------------------------------
    public function createStepOne(Request $request)
    {
        $item = $request->session()->get('item');
        $craftExist = $request->session()->get('craftExist');
//        $craft = new Craft() -------------------------------------------rewrite code to use in session craft model ???
        $types = Type::all();

        return view('admin.items.create-step-one', compact('item'))
            ->with('types', $types)
            ->with('craftExist', $craftExist);
    }

    public function postCreateStepOne(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:20',
            'description' => 'required|max:650',
            'type_id' => 'required|not_in:-1',
        ]);

        if (empty($request->session()->get('item'))) {
            $item = new Item();
            $item->fill($validatedData);
            $request->session()->put('item', $item);
        } else {
            $item = $request->session()->get('item');
            $item->fill($validatedData);
            $request->session()->put('item', $item);
        }

        if ($request->input('craftExist')) {
            $request->session()->put('craftExist', true);

            return redirect()->route('admin.items.create.step.two');

        } else {
            $request->session()->put('craftExist', false);
            $request->session()->forget('itemsForCraft');

            return redirect()->route('admin.items.create.step.three');
        }
    }

//-----------------Step Two---------------------------------------------------------------------------------------------
    public function createStepTwo(Request $request)
    {
        $item = $request->session()->get('item');
        $itemsForCraft = Item::all();
        $types = Type::all();

        $oldItemsForCraft = $request->session()->get('itemsForCraft');

        return view('admin.items.create-step-two', compact('item'))
            ->with('itemsForCraft', $itemsForCraft)
            ->with('types', $types)
            ->with('oldItemsForCraft', $oldItemsForCraft);
    }

    public function postCreateStepTwo(Request $request)
    {
        $craftRecipe = $request->validate([
            'position_1' => 'sometimes',
            'position_2' => 'sometimes',
            'position_3' => 'sometimes',
            'position_4' => 'sometimes',
            'position_5' => 'sometimes',
            'position_6' => 'sometimes',
            'position_7' => 'sometimes',
            'position_8' => 'sometimes',
            'position_9' => 'sometimes',
        ]);

        $itemsForCraft = null;
        foreach ($craftRecipe as $position => $itemId) {
            if ($itemId != -1) {
                $itemsForCraft[explode('_', $position)[1]] = $itemId;
            }
        }

        if ($itemsForCraft == null) {
            return redirect()->route('admin.items.create.step.two')
                ->with('warning',
                    'There must be crafting for this item.
                    If not, go back to the previous page and remove the checkbox for the crafting recipe');
        }

        $request->session()->put('itemsForCraft', $itemsForCraft);

        return redirect()->route('admin.items.create.step.three');
    }

//-----------------Step Three-------------------------------------------------------------------------------------------
    public function createStepThree(Request $request)
    {
        $item = $request->session()->get('item');
        $craftExist = $request->session()->get('craftExist');

        return view('admin.items.create-step-three', compact('item'))->with('craftExist', $craftExist);
    }

    public function postCreateStepThree(Request $request)
    {
        $request->validate([
            'image' => ['required', 'image:jpg, jpeg, png'],
        ]);
        $imageName = $request->file('image')->getClientOriginalName();
        $image_path = $request->file('image')->storeAs('items', $imageName);

        $item = $request->session()->get('item');

        $item->uuid = Str::uuid();
        $item->image = '/storage/'.$image_path;

        $item->save();

        if ($request->session()->get('craftExist')) {
            $craft = $request->session()->get('itemsForCraft');
            $craftForInsert = [];
            foreach ($craft as $position => $itemForCraftId) {
                $craftForInsert[] = [
                    'item_for_crafting' => $itemForCraftId,
                    'position' => $position,
                ];
            }
            $item->crafts()->createMany($craftForInsert);
            $request->session()->forget('itemsForCraft');
        }

        $request->session()->forget('item');
        $request->session()->forget('craftExist');

        return redirect()->route('admin.items.index');
    }

//-----------------Cancel Item Creation---------------------------------------------------------------------------------
    public function cancelCreateItem(Request $request)
    {
        $request->session()->forget('item');
        $request->session()->forget('itemsForCraft');
        $request->session()->forget('craftExist');

        return redirect()->route('admin.items.index');
    }
}
