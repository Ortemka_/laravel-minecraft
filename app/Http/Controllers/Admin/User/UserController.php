<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Mail\User\PasswordMail;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        $roles = User::getRoles();

        return view('admin.users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = User::getRoles();

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:20',
            'email' => 'required|max:30|email|unique:users',
            'role' => 'required|integer',
        ]);

        $password = Str::random(10);
        $data['password'] = Hash::make($password);

        $user = User::firstOrCreate(['email' => $data['email']], $data);
//        Mail::to($data['email'])->send(new PasswordMail($password));

//        event(new Registered($user));

        return to_route('admin.users.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $totalLikedItems = $user->likedItems()->count();
        $totalComments = $user->comments()->count();

        return view('admin.users.show', compact('user', 'totalLikedItems', 'totalComments'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $roles = User::getRoles();

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            'name' => 'required|max:20',
            'email' => 'required|max:30|email|unique:users,email,'.$request->user_id,
            'user_id' => 'required|integer|exists:users,id',
            'role' => 'required|integer|not_in:-1',
        ]);

        $user->update($data);

        return to_route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $user->delete();

        return to_route('admin.users.index')->with('success', ' Item moved to trash');
    }

    public function userLikedItems(User $user)
    {
        $items = $user->likedItems;

        return view('admin.users.liked-items', compact('user', 'items'));
    }

    public function userComments(User $user)
    {
        $comments = $user->comments;

        return view('admin.users.user-comments', compact('user', 'comments'));
    }
}
