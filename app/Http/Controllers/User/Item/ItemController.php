<?php

namespace App\Http\Controllers\User\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Livewire\WithPagination;

class ItemController extends Controller
{
    use WithPagination;

    public function index()
    {
//        $equippedItems = Item::withCount('likedUsers')->orderBy('equipped_users_count', 'DESC')->get()->take(2); // return items with a count of how many users take it to equipment
        $items = Item::latest('created_at')->paginate(15);

        return view('user.items.index', compact('items'));
    }

    public function show(Item $item)
    {
//        $relatedItems = Item::where('type_id', $item->type_id)
//            ->where('id', '!=', $item->id)
//            ->get()
//            ->take(3);

        $itemsForCraft = self::getItemsForCraft($item);

        return view('user.items.show', compact('item', 'itemsForCraft'));
    }

    public function getItemsForCraft(Item $item)
    {
        $itemsForCraft = [];

        foreach ($item->crafts()->get() as $craftItem) {
            $itemsForCraft[$craftItem->position] = Item::where('id', $craftItem->item_for_crafting)->firstOrFail();
        }

        return $itemsForCraft;
    }
}
