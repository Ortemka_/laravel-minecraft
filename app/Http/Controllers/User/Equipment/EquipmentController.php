<?php

namespace App\Http\Controllers\User\Equipment;

use App\Http\Controllers\Controller;
use App\Models\Item;

class EquipmentController extends Controller
{
    public function main()
    {
        $totalLikedItems = auth()->user()->likedItems()->count();
        $totalComments = auth()->user()->comments()->count();

        return view('user.equipment.main', compact('totalLikedItems', 'totalComments'));
    }

    public function likedItems()
    {
        $items = auth()->user()->likedItems;

        return view('user.equipment.liked-items', compact('items'));
    }

    public function likeItem(Item $item)
    {
        auth()->user()->likedItems()->toggle($item->id); // "toggle" add the ability to attach and detach an item

        return redirect()->back();
    }

    public function purchasedItems()
    {
        $items = auth()->user()->purchasedItems;

        return view('user.equipment.purchased-items', compact('items'));
    }
}
