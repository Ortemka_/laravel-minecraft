<?php

namespace App\Http\Controllers\User\Comment;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Item;
use function auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function redirect;
use function view;

class CommentController extends Controller
{
    public function index()
    {
        $comments = auth()->user()->comments;

        return view('user.comments.index', compact('comments'));
    }

    public function store(Request $request, Item $item)
    {
        $request->validate([
            'comment' => 'required|min:10',
        ]);

        $comment = new Comment();
        $comment->user_id = Auth::id();
        $comment->message = $request->comment;

        $item->comments()->save($comment);

        return redirect()->route('user.items.show', $item->id)
            ->with('success', 'Thank for your comment');
    }

    public function edit(Comment $comment, Item $item)
    {
        return view('user.comments.edit', compact('comment', 'item'));
    }

    public function update(Request $request, Comment $comment)
    {
        $data = $request->validate([
            'message' => 'required',
        ]);

        $comment->update($data);

        return redirect()->route('user.items.show', $comment->item_id)
            ->with('success', 'Your comment has been updated');
    }

    public function delete(Request $request, Comment $comment)
    {
        $comment->delete();

        return redirect()->route('user.items.show', $request->item_id)
            ->with('success', 'Your comment has been deleted');
    }
}
