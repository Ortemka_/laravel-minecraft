<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'title' => $this->title,
            'description' => $this->description,
            //            'type' => new ItemTypeResource($this->type),
            'type' => $this->type->title,
            $this->mergeWhen($request->routeIs('items.show'), [
                'price' => $this->price,
                'comments' => ItemCommentResource::collection($this->comments),
            ]),
        ];
    }
}
