<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ((int) auth()->user()->role !== User::ROLE_ADMIN) { // This code not work because Null in user role transfromed in integer = 0, means 0 == 0 (ROLE_ADMIN)
            abort(404);
        }

        return $next($request);
    }
}
