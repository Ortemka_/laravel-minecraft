<?php

namespace App\Http\Livewire;

use App\Models\Item;
use App\Models\ShoppingCart;
use Livewire\Component;
use Livewire\WithPagination;

class ItemList extends Component
{
    use WithPagination;

    public function addToCart($itemId)
    {
        if (auth()->user()) {
            $data = [
                'user_id' => auth()->user()->id,
                'item_id' => $itemId,
            ];

            ShoppingCart::updateOrCreate($data);

            $this->emit('updateCartCount');

            session()->flash('success', 'Item added to cart');
        } else {
            return redirect('login');
        }
    }

    public function render()
    {
        $items = Item::latest()->paginate(15);

        return view('livewire.item-list', compact('items'));
    }
}
