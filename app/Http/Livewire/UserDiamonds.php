<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UserDiamonds extends Component
{
    public $diamonds;

    protected $listeners = ['updateDiamondsCount' => 'getUserDiamonds'];

    public function getUserDiamonds()
    {
        $this->diamonds = auth()->user()->resource->diamonds;
    }

    public function render()
    {
        $this->getUserDiamonds();

        return view('livewire.user-diamonds');
    }
}
