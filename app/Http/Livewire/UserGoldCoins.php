<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UserGoldCoins extends Component
{
    public $goldCoins;

    protected $listeners = ['updateGoldCoinsCount' => 'getUserGoldCoins'];

    public function getUserGoldCoins()
    {
        $this->goldCoins = auth()->user()->resource->gold_coins;
    }

    public function render()
    {
        $this->getUserGoldCoins();

        return view('livewire.user-gold-coins');
    }
}
