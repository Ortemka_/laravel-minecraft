<?php

namespace App\Http\Livewire;

use App\Models\Item;
use App\Models\ShoppingCart;
use App\Models\Type;
use Livewire\Component;

class ItemListByTypes extends Component
{
    public $selectedItem = null;

    public $itemsForCraft = null;

    protected $listeners = [
        'itemSelected' => 'itemSelected',
    ];

    public function itemSelected($itemId)
    {
        $this->selectedItem = Item::with('crafts')->find($itemId);
        $this->itemsForCraft = self::getItemsForCraft($this->selectedItem->crafts);
    }

    protected function getItemsForCraft($itemsForCraft)
    {
        $sortItemsForCraft = [];

        foreach ($itemsForCraft as $craftItem) {
            $sortItemsForCraft[$craftItem->position] = Item::find($craftItem->item_for_crafting);
        }

        return $sortItemsForCraft;
    }

    public function addToCart($itemId)
    {
        if (auth()->user()) {
            $data = [
                'user_id' => auth()->user()->id,
                'item_id' => $itemId,
            ];

            ShoppingCart::updateOrCreate($data);

            $this->emit('updateCartCount');

            session()->flash('success', 'Item added to cart');

        } else {
            return redirect('login');
        }
    }

    public function render()
    {
        $types = Type::with('items')->get();

        return view('livewire.item-list-by-types', compact('types'));
    }
}
