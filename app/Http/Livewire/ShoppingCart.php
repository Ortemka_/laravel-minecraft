<?php

namespace App\Http\Livewire;

use App\Models\ShoppingCart as Cart;
use Livewire\Component;

class ShoppingCart extends Component
{
    public $cartItems;

    public $total = 0;

    public function incrementQty($id)
    {
        $cart = Cart::whereId($id)->first();
        $cart->quantity++;

        $cart->save();

        session()->flash('success', 'Item quantity update');
    }

    public function decrementQty($id)
    {
        $cart = Cart::whereId($id)->first();

        if ($cart->quantity > 1) {
            $cart->quantity--;
            $cart->save();

            session()->flash('success', 'Item quantity update');
        } else {
            session()->flash('warning', 'Item quantity cannot be less then 1');

        }
    }

    public function removeItem($id)
    {
        $cart = Cart::whereId($id)->first();

        if ($cart) {
            $cart->delete();
            $this->emit('updateCartCount');
        }

        session()->flash('success', 'Item delete');
    }

    public function confirmPurchase()
    {
        $user = auth()->user();
        if ($user->resource->gold_coins >= $this->total) {
            $user->resource->gold_coins -= $this->total;
            $user->resource->save();  // works but needs a redirect to update user resource or change
            $this->emit('updateGoldCoinsCount');

            session()->flash('success', 'Purchase was successful');
        } else {
            session()->flash('warning', 'Insufficient funds to purchase');

        }
    }

    public function render()
    {
        $cartItems = $this->cartItems = Cart::with('item')->where(['user_id' => auth()->user()->id])->get();
        $totalPrice = 0;
        foreach ($cartItems as $cartItem) {
            $totalPrice += $cartItem->item->price * $cartItem->quantity;
        }
        $this->total = $totalPrice;

        return view('livewire.shopping-cart');
    }
}
