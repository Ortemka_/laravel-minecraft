<?php

use App\Http\Controllers\Admin\Comment\CommentController as AdminCommentController;
use App\Http\Controllers\Admin\Item\ItemController as AdminItemController;
use App\Http\Controllers\Admin\Item\ItemMultiStepFormController as AdminItemMultiStepFormController;
use App\Http\Controllers\Admin\Item\TrashedItemController as AdminTrashedItemController;
use App\Http\Controllers\Admin\User\UserController as AdminUserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\User\Comment\CommentController;
use App\Http\Controllers\User\Equipment\EquipmentController;
use App\Http\Controllers\User\Item\ItemController as UserItemController;
use App\Http\Livewire\ItemListByTypes as UserItemListByTypes;
use App\Http\Livewire\ShoppingCart as UserShoppingCart;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::prefix('/admin')->name('admin.')->middleware(['auth', 'admin'])->group(function () {

    Route::prefix('/items')->name('items.')->group(function () {
        Route::get('/create-step-one', [AdminItemMultiStepFormController::class, 'createStepOne'])
            ->name('create.step.one');
        Route::post('/create-step-one', [AdminItemMultiStepFormController::class, 'postCreateStepOne'])
            ->name('create.step.one.post');
        Route::get('/create-step-two', [AdminItemMultiStepFormController::class, 'createStepTwo'])
            ->name('create.step.two');
        Route::post('/create-step-two', [AdminItemMultiStepFormController::class, 'postCreateStepTwo'])
            ->name('create.step.two.post');
        Route::get('/create-step-three', [AdminItemMultiStepFormController::class, 'createStepThree'])
            ->name('create.step.three');
        Route::post('/create-step-three', [AdminItemMultiStepFormController::class, 'postCreateStepThree'])
            ->name('create.step.three.post');
        Route::get('/create-cancel', [AdminItemMultiStepFormController::class, 'cancelCreateItem'])
            ->name('create.cancel');
    });

    Route::resource('/items', AdminItemController::class);

    Route::prefix('/trashed')->name('trashed.')->group(function () {
        Route::get('/', [AdminTrashedItemController::class, 'index'])->name('index');
        Route::get('/{item}', [AdminTrashedItemController::class, 'show'])->name('show')->withTrashed();
        Route::patch('/{item}', [AdminTrashedItemController::class, 'update'])->name('update')->withTrashed();
        Route::delete('/{item}', [AdminTrashedItemController::class, 'destroy'])->name('destroy')->withTrashed();
    });

    Route::prefix('/users')->name('users.')->group(function () {
        Route::get('/{user}/items', [AdminUserController::class, 'userLikedItems'])->name('liked.items');
        Route::get('/{user}/comments', [AdminUserController::class, 'userComments'])->name('comments');
    });

    Route::resource('/users', AdminUserController::class);

    Route::prefix('/comments')->name('comments.')->group(function () {
        Route::delete('/delete/{comment}', [AdminCommentController::class, 'delete'])->name('delete');
    });

});

Route::prefix('/user')->name('user.')->middleware('auth')->group(function () { // add middleware authenticated user

    Route::prefix('/items')->name('items.')->group(function () {
        Route::get('/', function () {
        return view('user.items.item-list');
        })->name('index');
        Route::get('/item-by-types', function () {
        return view('user.items.items-by-types');
        })->name('list-by-types');
//        Route::get('/item-by-types', UserItemListByTypes::class)->name('list-by-types');
        Route::get('/{item}', [UserItemController::class, 'show'])->name('show');
    });

    Route::get('/shopping-cart', function () {
    return view('user.items.shopping-cart');
    })->name('shopping-cart');
//    Route::get('/shopping-cart', UserShoppingCart::class)->name('shopping-cart');

    Route::prefix('/equipment')->name('equipment.')->group(function () {
        Route::get('/main', [EquipmentController::class, 'main'])->name('main');
        Route::get('/liked/items', [EquipmentController::class, 'likedItems'])->name('liked.items');
        Route::put('/like/{item}', [EquipmentController::class, 'likeItem'])->name('like.item');
        Route::get('/purchased/items', [EquipmentController::class, 'purchasedItems'])->name('purchased.items');
    });

    Route::prefix('/comment')->name('comment.')->group(function () {
        Route::get('/', [CommentController::class, 'index'])->name('index');
        Route::put('/store/{item}', [CommentController::class, 'store'])->name('store');
        Route::get('/{comment}', [CommentController::class, 'edit'])->name('edit');
        Route::patch('/update/{comment}', [CommentController::class, 'update'])->name('update');
        Route::delete('/delete/{comment}', [CommentController::class, 'delete'])->name('delete');
    });
});

require __DIR__.'/auth.php';
